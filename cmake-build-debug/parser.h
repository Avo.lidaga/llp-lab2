/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_INT = 258,
     T_TYPE_BOOL = 259,
     T_TYPE_TEXT = 260,
     T_TYPE_FLOAT32 = 261,
     T_TYPE_INT32 = 262,
     T_FLOAT = 263,
     T_STRING = 264,
     T_NAME = 265,
     T_BOOL = 266,
     T_LESS_EQUALS = 267,
     T_GREATER_EQUALS = 268,
     T_LESS = 269,
     T_GREATER = 270,
     T_EQUALS = 271,
     T_NEQUALS = 272,
     T_LEFT = 273,
     T_RIGHT = 274,
     T_COMMA = 275,
     T_DOT = 276,
     T_SEMICOLON = 277,
     T_NOT = 278,
     T_OR = 279,
     T_AND = 280,
     T_LIMIT = 281,
     T_OFFSET = 282,
     T_VALUES = 283,
     T_INTO = 284,
     T_SET = 285,
     T_CREATE = 286,
     T_TABLE = 287,
     T_ON = 288,
     T_JOIN = 289,
     T_WHERE = 290,
     T_DELETE = 291,
     T_INSERT = 292,
     T_UPDATE = 293,
     T_SELECT = 294,
     T_FROM = 295
   };
#endif
/* Tokens.  */
#define T_INT 258
#define T_TYPE_BOOL 259
#define T_TYPE_TEXT 260
#define T_TYPE_FLOAT32 261
#define T_TYPE_INT32 262
#define T_FLOAT 263
#define T_STRING 264
#define T_NAME 265
#define T_BOOL 266
#define T_LESS_EQUALS 267
#define T_GREATER_EQUALS 268
#define T_LESS 269
#define T_GREATER 270
#define T_EQUALS 271
#define T_NEQUALS 272
#define T_LEFT 273
#define T_RIGHT 274
#define T_COMMA 275
#define T_DOT 276
#define T_SEMICOLON 277
#define T_NOT 278
#define T_OR 279
#define T_AND 280
#define T_LIMIT 281
#define T_OFFSET 282
#define T_VALUES 283
#define T_INTO 284
#define T_SET 285
#define T_CREATE 286
#define T_TABLE 287
#define T_ON 288
#define T_JOIN 289
#define T_WHERE 290
#define T_DELETE 291
#define T_INSERT 292
#define T_UPDATE 293
#define T_SELECT 294
#define T_FROM 295




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 10 "/Users/malikaagadilova/Desktop/llp/llp-lab2/parser.y"
{
    struct AstNode *node;
	int ival;
	float fval;
	int bval;
	char* sval;
}
/* Line 1529 of yacc.c.  */
#line 137 "/Users/malikaagadilova/Desktop/llp/llp-lab2/cmake-build-debug/parser.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

